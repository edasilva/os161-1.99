#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include "opt-A2.h"
#include <mips/trapframe.h>
#include <clock.h>
#include "opt-A3.h"
#include <vfs.h>
#include <kern/fcntl.h>

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

#if OPT_A2

int numChildren = array_num(p->p_children);

  for(int i = 0; i < numChildren; i++) {

    struct proc *temp_child = (struct proc *)array_get(p->p_children, 0);
    array_remove(p->p_children, 0);

    spinlock_acquire(&temp_child->p_lock);
    if(WIFEXITED(temp_child->p_exitstatus)) {
      spinlock_release(&temp_child->p_lock);
      proc_destroy(temp_child);
    } else {
      temp_child->p_parent = NULL;
      spinlock_release(&temp_child->p_lock);
    }

  }
  

#endif /* OPT_A2*/

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
#if OPT_A2
  //removed the proc_destroy call 
  //get the lock for the proc structure
  spinlock_acquire(&p->p_lock);
  //exitstatus of 1 indicates running 
  if((p->p_parent != NULL) && !(WIFEXITED(p->p_parent->p_exitstatus)))  {
    //set exitstatus to indicate exited  -- using macros from wait.h
    p->p_exitstatus = _MKWAIT_EXIT(exitcode);
    //add exitcode to the proc struct 
    p->p_exitcode = exitcode;
    spinlock_release(&p->p_lock);
  } else {
    spinlock_release(&p->p_lock);
    proc_destroy(p);  
  }
#else 
  proc_destroy(p);  
#endif /*OPT_A2*/
  
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  /* retval is the PID of the current process */
  #if OPT_A2
    *retval = curproc->p_pid;
  #else 
    *retval = 1;
  #endif /* OPT_A2 */

  return(0);
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }

  #if OPT_A2 
  int index = 0; 
  struct proc *temp_child; 
  int notFound = 0;

  int numChildren = array_num(curproc->p_children); 
  while(index < numChildren ) {
    pid_t temp_pid = ((struct proc *)array_get(curproc->p_children, index))->p_pid; 

    if(temp_pid == pid) {
      notFound = 1; 
      temp_child = (struct proc *)array_get(curproc->p_children, index);
      array_remove(curproc->p_children, index);
      break; 
    } 

    else {
      index++; 
    }
  }
  
  if(notFound == 0) {
    *retval = -1; 
    return(ESRCH); 
  }

  spinlock_acquire(&temp_child->p_lock);
   
  while(!temp_child->p_exitstatus) {
    spinlock_release(&temp_child->p_lock);
    clocksleep(1);
    spinlock_acquire(&temp_child->p_lock); 
  }
  spinlock_release(&temp_child->p_lock); 

  exitstatus = _MKWAIT_EXIT(temp_child->p_exitcode); 
  proc_destroy(temp_child); 
  
  #endif /*OPT_A2*/

  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}

#if OPT_A2

int
sys_fork(int *retval,
        struct trapframe *tf) {
  
  pid_t parentpid;
  parentpid = curproc->p_pid; 

  //create new child process structure 
  struct proc * child = proc_create_runprogram("child");
  child->p_parent = curproc; //set parent process of the child to be the current process
  //add pointer to child proc struct to parent proc 
  array_add(curproc->p_children, child, NULL);
  //set return value to be PID of the child
  *retval = child->p_pid;
  //copy address space of parent to child 
  as_copy(curproc_getas(), &child->p_addrspace); 
  //allocate a new trapframe for child
  struct trapframe *tf_child = (struct trapframe *)kmalloc(sizeof(struct trapframe));
  //copy contents of current trapframe to new child trapframe
  tf_child->tf_vaddr = tf->tf_vaddr;
  tf_child->tf_status = tf->tf_status;
  tf_child->tf_cause = tf->tf_cause;
  tf_child->tf_lo = tf->tf_lo;
  tf_child->tf_hi = tf->tf_hi;
  tf_child->tf_ra = tf->tf_ra;
  tf_child->tf_at = tf->tf_at;
  tf_child->tf_v0 = tf->tf_v0;
  tf_child->tf_v1 = tf->tf_v1;
  tf_child->tf_a0 = tf->tf_a0;
  tf_child->tf_a1 = tf->tf_a1;
  tf_child->tf_a2 = tf->tf_a2;
  tf_child->tf_a3 = tf->tf_a3;
  tf_child->tf_t0 = tf->tf_t0;
  tf_child->tf_t1 = tf->tf_t1;
  tf_child->tf_t2 = tf->tf_t2;
  tf_child->tf_t3 = tf->tf_t3;
  tf_child->tf_t4 = tf->tf_t4;
  tf_child->tf_t5 = tf->tf_t5;
  tf_child->tf_t6 = tf->tf_t6;
  tf_child->tf_t7 = tf->tf_t7;
  tf_child->tf_s0 = tf->tf_s0;
  tf_child->tf_s1 = tf->tf_s1;
  tf_child->tf_s2 = tf->tf_s2;
  tf_child->tf_s3 = tf->tf_s3;
  tf_child->tf_s4 = tf->tf_s4;
  tf_child->tf_s5 = tf->tf_s5;
  tf_child->tf_s6 = tf->tf_s6;
  tf_child->tf_s7 = tf->tf_s7;
  tf_child->tf_t8 = tf->tf_t8;
  tf_child->tf_t9 = tf->tf_t9;
  tf_child->tf_k0 = tf->tf_k0;
  tf_child->tf_k1 = tf->tf_k1;
  tf_child->tf_gp = tf->tf_gp;
	tf_child->tf_sp = tf->tf_sp;
  tf_child->tf_s8 = tf->tf_s8;
  tf_child->tf_epc = tf->tf_epc;


  
  //call thread fork
  thread_fork("child_thread", child, enter_forked_process, (void *)tf_child, 0);

  clocksleep(1);

  return 0;
  
}

#endif /* OPT_A2*/

#if OPT_A3

int 
sys_execv(char *progname, char **user_args) {

	struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
	int result;
  int argc = 0;
  
  //count number of arugments in argv

	/* Open the file. */
	result = vfs_open(progname, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	/* We should be a new process. */
	//KASSERT(curproc_getas() == NULL);

	/* Create a new address space. */
	as = as_create();
	if (as ==NULL) {
		vfs_close(v);
		return ENOMEM;
	}

  //copy in args from userspace
  char **argv = args_alloc(); 
  argc = argcopy_in(&argv, user_args); //pass by reference 

  //destroy old address space 
  as_destroy(curproc_getas()); 

	/* Switch to it and activate it. */
	curproc_setas(as);
	as_activate();

	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}

	//allocate and set the arugments to pass to the user 
		vaddr_t *argv_user = kmalloc((argc + 1) * sizeof(vaddr_t)); 
		argv_user[argc] = (vaddr_t)NULL; //NULL terminated 

		for(int i = 0; i < argc; i++) {
			vaddr_t arg_addr = argcopy_out(&stackptr, argv[i]); 
			argv_user[i] = arg_addr; 
		}

		stackptr -= (argc + 1) * sizeof(vaddr_t);
		if(stackptr % 4 != 0){
				stackptr -= (stackptr % 4); 
		}
		copyout(argv_user, (userptr_t)stackptr, (argc + 1) * sizeof(vaddr_t)); 

		vaddr_t start_argv_user = stackptr; 
		

		kfree(argv_user);
    args_free(argv); 
	/* Warp to user mode. */
	enter_new_process(argc /*argc*/, (userptr_t)start_argv_user /*userspace addr of argv*/,
			  stackptr, entrypoint);

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;

  return 0;
}


#endif /*OPT_A3*/ 
